// Requiring all the files Gulp needs for processing the files
var gulp = require( 'gulp' ),
	sass = require( 'gulp-ruby-sass' ),
	autoprefixer = require( 'gulp-autoprefixer' ),
	yuidoc = require( 'gulp-yuidoc' );

// Caching all the locations and destinations for the various files.
var jsSrc = 'www/js/**/*.js',
	jsDest = 'www/js/',
	scssSrc = 'www/scss/**/*.scss',
	scssDest = 'www/css';

gulp.task( 'scripts', function () {

	gulp.src( jsSrc )
		.pipe( gulp.dest( jsDest ) );

} );


gulp.task( 'styles', function () {

	gulp.src( scssSrc )
		.pipe( autoprefixer( { browsers : [ 'last 15 versions' ] } ) )
		.on( 'error', function ( error ) {
			console.log( '[ERROR] Styles : ' + error.message );
		} )
		.pipe( sass( { style : 'compressed', sourcemapPath : 'build/css' } ) )
		.on( 'error', function ( error ) {
			console.log( '[ERROR] Styles : ' + error.message );
		} )
		.pipe( gulp.dest( scssDest ) );

} );

gulp.task( 'watch', function () {
	//gulp.watch( jsSrc, [ 'scripts' ] );
	gulp.watch( scssSrc, [ 'styles' ] );
} );

gulp.task( 'doc', function () {
	gulp.src( 'www/js/app/**/*.js')
		.pipe(yuidoc({
			project: {
				name: 'Affiliate App',
				description: 'This is documentation for the Affiliate App. ( Better description coming )',
				version: '1.0'
			}
		}))
		.pipe(gulp.dest('../_docs'));
} );

/*
 * Once things are all set up, run 'gulp' in the terminal.
 * This will run all the tasks in the array.
 */
gulp.task( 'default', [ 'styles', 'watch' ] );
//gulp.task( 'default', [ 'scripts', 'styles', 'watch' ] );
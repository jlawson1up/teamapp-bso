/**
 * AppConfig
 * Meant to hold all business logic that is specific to the individual app *
 *
 * @type
 */
//feed:[APP ID}-[FEED NAME]-rss-feed
var AppConfig = {
	BASE_URL        : 'http://sports-data-develop.herokuapp.com/api/v2/content?',
	RSS_BASE        : '/content?tag=feed:',
	TEAM_KEY        : 'black-sports-online',
	AFFILIATE_NAME  : 'BSO',
	PLACEHOLDER_IMG : 'images/placeHolder.jpg',
	CACHE_TIMEOUT   : 300000, // 900000 = 15 minutes in milliseconds
	LOGO_URL        : 'images/logo-white.png'
};

AppConfig.ENDPOINTS = {
	HOME_CONTENT             : AppConfig.BASE_URL + 'tag=feed:' + AppConfig.TEAM_KEY + '-home-rss-feed',
	COLLEGE_BASKETBALL_RSS   : AppConfig.BASE_URL + 'tag=feed:' + AppConfig.TEAM_KEY + '-college-basketball-rss-feed',
	COLLEGE_FOOTBALL_RSS     : AppConfig.BASE_URL + 'tag=feed:' + AppConfig.TEAM_KEY + '-college-football-rss-feed',
	VIDEOS_RSS               : AppConfig.BASE_URL + 'tag=feed:' + AppConfig.TEAM_KEY + '-videos-rss-feed',
	NBA_RSS                  : AppConfig.BASE_URL + 'tag=feed:' + AppConfig.TEAM_KEY + '-nba-rss-feed',
	NFL_RSS                  : AppConfig.BASE_URL + 'tag=feed:' + AppConfig.TEAM_KEY + '-nfl-rss-feed',
	GUMBO_RSS                : AppConfig.BASE_URL + 'tag=feed:' + AppConfig.TEAM_KEY + '-sports-gumbo-rss-feed',
	SPORTS_ENTERTAINMENT_RSS : AppConfig.BASE_URL + 'tag=feed:' + AppConfig.TEAM_KEY + '-sports-entertainment-rss-feed',
	BOXING_UFC_WWE_RSS       : AppConfig.BASE_URL + 'tag=feed:' + AppConfig.TEAM_KEY + '-boxing-ufc-wwe-rss-feed',
	PRETTY_LADIES            : AppConfig.BASE_URL + 'tag=feed:' + AppConfig.TEAM_KEY + '-pretty-ladies-rss-feed',
	TWITTER_FEED             : AppConfig.BASE_URL + 'tag=' + AppConfig.TEAM_KEY + '&tag=source:twitter&tag=type:article',
	FACEBOOK_FEED            : AppConfig.BASE_URL + 'tag=' + AppConfig.TEAM_KEY + '&tag=source:facebook&tag=type:article'
};

AppConfig.CONTENT = [
	{
		icon     : 'images/icon-home.png',
		title    : 'Home',
		layout   : 'home',
		sections : [
			{ type: 'news', titleBold : 'Hero', titleNormal : 'Section', endPoint : AppConfig.ENDPOINTS.HOME_CONTENT, limit : 4, style : 'grid' },
			{ type: 'news', titleBold : 'BSO', titleNormal : 'Now', endPoint : AppConfig.ENDPOINTS.HOME_CONTENT, limit : 10, start : 4 },
			{ type: 'news', titleBold : 'NFL', titleNormal : 'News', endPoint : AppConfig.ENDPOINTS.NFL_RSS, limit : 10, start : 4 },
			{ type: 'news', titleBold : 'NBA', titleNormal : 'News', endPoint : AppConfig.ENDPOINTS.NBA_RSS, limit : 10, start : 0 },
			{ type: 'news', titleBold : 'BSO', titleNormal : 'Video', endPoint : AppConfig.ENDPOINTS.VIDEOS_RSS, limit : 10, start : 0 },
			{ type: 'social', subType: 'twitter', titleBold : 'BSO', titleNormal : 'Conversations', endPoint : AppConfig.ENDPOINTS.TWITTER_FEED, limit : 10, start : 0 }
		]
	},
	{
		icon     : 'images/icon-chat.png',
		title    : 'News',
		layout   : 'content',
		sections : [
			{ titleBold : 'Sports', titleNormal : 'Entertainment', endPoint : AppConfig.ENDPOINTS.SPORTS_ENTERTAINMENT_RSS },
			{ titleBold : 'Pretty', titleNormal : 'Ladies', endPoint : AppConfig.ENDPOINTS.PRETTY_LADIES },
			{ titleBold : 'NFL', titleNormal : 'News', endPoint : AppConfig.ENDPOINTS.NFL_RSS },
			{ titleBold : 'College', titleNormal : 'Football', endPoint : AppConfig.ENDPOINTS.COLLEGE_FOOTBALL_RSS },
			{ titleBold : 'College', titleNormal : 'Basketball', endPoint : AppConfig.ENDPOINTS.COLLEGE_BASKETBALL_RSS },
			{ titleBold : 'Boxing', titleNormal : 'UFC - WWE', endPoint : AppConfig.ENDPOINTS.BOXING_UFC_WWE_RSS },
			{ titleBold : 'Sports', titleNormal : 'Gumbo', endPoint : AppConfig.ENDPOINTS.GUMBO_RSS }
		]
	},
	{
		icon     : 'images/icon-more.png',
		title    : 'Video',
		layout   : 'content',
		sections : [
			{ type: 'video', titleBold : 'BSO', titleNormal : 'Video', endPoint : AppConfig.ENDPOINTS.VIDEOS_RSS, limit : 20, start : 0 }
		]
	},

	{
		icon     : 'images/icon-media.png',
		title    : 'Social',
		layout   : 'social',
		sections : [
			{ type: 'social', subType : 'twitter', titleBold : 'Twitter', titleNormal : '', endPoint : AppConfig.ENDPOINTS.TWITTER_FEED },
			{ type: 'social', subType : 'facebook', titleBold : 'Facebook', titleNormal : '', endPoint : AppConfig.ENDPOINTS.FACEBOOK_FEED }
		]
	},
	{
		icon     : 'images/icon-more.png',
		title    : 'More',
		layout   : 'more',
		sections : [
			{ titleBold : 'More', titleNormal : '', endPoint : '' }
		]
	}

];
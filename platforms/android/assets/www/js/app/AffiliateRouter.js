angular.module( 'AffiliateApp' )
	.config( function ( $routeProvider ) {
		$routeProvider
			.when( '/', {
				redirectTo : '/home/' + AppConfig.CONTENT[ 0 ].title
			} )
			.when( '/home/:sectionId', {
				templateUrl : 'js/app/views/HomeDisplay.html',
				controller  : 'HomeCtrl as homeCtrl'
			} )
			.when( '/content/:sectionId', {
				templateUrl : 'js/app/views/ContentsDisplay.html',
				controller  : 'ContentCtrl as contentCtrl'
			} )
			.when( '/social/:sectionId', {
				templateUrl : 'js/app/views/SocialDisplay.html',
				controller  : 'SocialCtrl as socialCtrl'
			} )
			.when( '/:sectionId/:subsectionIndex/article/:articleIndex', {
				templateUrl : 'js/app/views/ArticleDisplay.html',
				controller  : 'ArticleCtrl as articleCtrl'
			} );
	} );
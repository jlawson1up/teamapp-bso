angular.module( 'affiliate.home', [
	'affiliate.modules.articles'
] )

/**
 * Connects data to the home view
 */
	.controller( 'HomeCtrl', function ( $scope, $routeParams, ArticlesModel ) {
		var homeCtrl = this;
		console.log( 'In HomeCtrl' );

		homeCtrl.content = _.findWhere( $scope.appContent, { title : $routeParams.sectionId } );

		/**
		 * Call to the ArticlesModel to fetch all the content from the endpoints
		 */
		ArticlesModel.getArticles( _.pluck( homeCtrl.content.sections, 'endpoint' ) )
			.then( function ( data ) {
				_.each( homeCtrl.content.sections, function ( object, index ) {
					object.articles = data[ index ];
				} );
				console.log( homeCtrl.content );
			} );



	} );
angular.module( 'AffiliateApp', [
	'ngSanitize',
	'ngRoute',
	'affiliate.home',
	'affiliate.contents',
	'affiliate.socials',
	'affiliate.factories',
	'affiliate.directives',
	'affiliate.article'
] )

/**
 * GlobalCtrl:
 *
 * This gathers all the information need to run this app.
 * Its main function is to discover the UI and display accordingly
 * Its secondary goal is to gather content endpoint information
 * and make it available to the rest of the app.
 *
 *
 */
	.controller( 'GlobalCtrl', function ( $scope, $templateCache, $location, $sce, ArticlesModel, AppCache ) {
		console.log( 'in global controller' );

		// need a global object that i can reference,
		// main purpose is to not re-animate items that have already
		$scope.isLoaded = {};
		$scope.isRandom = {};

		$scope.appContent = _.map( AppConfig.CONTENT, function ( content ) {
			return {
				title    : content.title,
				class    : content.title.toLowerCase().replace( ' ', '-' ),
				layout   : content.layout,
				sections : _.map( content.sections, function ( section ) {

					/**
					 * I ran into a situation where not checking for this caused issues.
					 * Example is Twitter. I want a class name for the section on screen.
					 * Without this logic I got a class like 'twitter-'
					 * And presents an issue if titleNormal is the only thing filled out.
					 * In that case I would get '-twitter'
					 */
					var cssClass;
					if ( section.titleBold && !section.titleNormal ) {
						cssClass = section.titleBold.toLowerCase().replace( ' ', '-' );
					} else if ( !section.titleBold && section.titleNormal ) {
						cssClass = section.titleNormal.toLowerCase().replace( ' ', '-' );
					} else {
						cssClass = section.titleBold.toLowerCase().replace( ' ', '-' ) + '-' + section.titleNormal.toLowerCase().replace( ' ', '-' )
					}

					return {
						type        : section.type,
						subType     : section.subType,
						title       : section.titleBold + ' ' + section.titleNormal,
						titleBold   : section.titleBold,
						titleNormal : section.titleNormal,
						class       : cssClass,
						endpoint    : section.endPoint
					}
				} )
			};
		} );

		$scope.getRandomSize = function () {
			return (Math.round( Math.random() ) == 0 ) ? 'small' : 'big';
		};

		$scope.gotoArticle = function ( section, subsection, article ) {
			console.log( section + ' : section' + subsection + ' : subsection | ' + article + ' : article' );
			var path = '/' + section + '/' + subsection + '/article/' + article;
			$location.path( path );
		};

		$scope.trustHtml = function ( article ) {
			return $sce.trustAsHtml( article );
		};

		/**
		 * Basic App Globals
		 */
		$scope.title = AppConfig.AFFILIATE_NAME;
		$scope.logo = AppConfig.LOGO_URL;
		$scope.urlMap = {
			homeGrid : 'home',
			social   : 'social',
			content  : 'content',
			setting  : 'settings',
			video    : 'video',
			podcast  : 'podcast'
		};

		/**
		 * This is the Global Timer to clear cache every 15 minutes.
		 * Time is set through the config
		 * @type {AppConfig.CACHE_TIMEOUT|*}
		 */
		var cacheTimeout = AppConfig.CACHE_TIMEOUT;
		setInterval( function () {
			AppCache.removeAll();
			$scope.isLoaded = {};
			$scope.isRandom = {};
			console.log( $scope.isLoaded, 'cache should be cleared' );
		}, cacheTimeout );
	} )

;
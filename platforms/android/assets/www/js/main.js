<<<<<<< HEAD
var $window = $( window ),
<<<<<<< HEAD
	$superContainer = $( '.container-super' ),
	modifier = 1.5,
	per;

$( function () {
	console.log( 'DOM ready' );

	initHandlebars();

	createDragger( $( '.slider--news' ) );
	createDragger( $( '.slider--entertainment' ) );
	createDragger( $( '.slider--nfl' ) );
	createDragger( $( '.slider--nba' ) );

	initParallax();


	////////////////////////////////////

	$( '.content--grid' ).each( function () {
		console.log( $( this ).offset() );
		var elementOffset = $( this ).offset();
		console.log( elementOffset + ' : elementOffset' );
		var offset = elementOffset.left + elementOffset.top;
		console.log( offset + ' : offset ' );
		var delay = ( offset / 1000 ) + 1;
		console.log( delay + ' : delay' );

		TweenMax.from( $( this ), 0.5, { scale : 0, delay : delay, ease : Back.easeOut } );
	} );


} );

function initParallax() {

	Draggable.create( '.container-dragger', {
		//type          : 'scrollTop',
		type          : 'scrollTop',
		bounds        : 'body',
		throwProps    : true,
		onDrag        : function () {
			customParallax( -this.y );
		},
		onThrowUpdate : function () {
			customParallax( -this.y );
		}
	} );

	console.log( 'initParallax' );
}

function customParallax( windowScrolled ) {
	var scrlPrct = windowScrolled / 1000;
	per = Math.floor( 100 * scrlPrct ) - 25;
	console.log( per + ' : per' );
	if ( per < 90 ) {
		$superContainer.css( { 'top' : (-per * modifier) + 'px' } );
	} else {
		$superContainer.css( { 'top' : '-90px' } );
	}
}


function initHandlebars() {

	/*var heroSource = $( '#hero-section' ).html(),
	 heroTemplate = Handlebars.compile( heroSource ),
	 heroContext = {
	 "category" : "hero",
	 "content"  : [
	 {
	 "meta"     : "College Football",
	 "headline" : "Bama safety landon collins declares for NFL draft",
	 "image"    : "images/placeholders/top_slider_01.jpg"
	 },
	 {
	 "meta"     : "College Football",
	 "headline" : "main title text headline number three.....",
	 "image"    : "images/placeholders/top_slider_02.jpg"
	 },
	 {
	 "meta"     : "College Football",
	 "headline" : "main title text headline number four.....",
	 "image"    : "images/placeholders/top_slider_03.jpg"
	 },
	 {
	 "meta"     : "College Football",
	 "headline" : "main title text headline number five.....",
	 "image"    : "images/placeholders/top_slider_04.jpg"
	 },
	 {
	 "meta"     : "College Football",
	 "headline" : "main title text headline number six.....",
	 "image"    : "images/placeholders/top_slider_05.jpg"
	 }
	 ]
	 },
	 heroHTML = heroTemplate( heroContext );

	 $( '.slider--hero' ).html( heroHTML );*/

	var breakingNews = $( '.section--news' ).html(),
		breakingTemplate = Handlebars.compile( breakingNews ),
		breakingContent = {
			"category" : "news",
			"content"  : [
				{
					"meta"     : "By Some Guy &bull; Thursday, January 9th, 2015",
					"headline" : "This is title 1",
					"image"    : "images/placeholders/news_01.jpg"
				},
				{
					"meta"     : "By Some Guy &bull; Thursday, January 9th, 2015",
					"headline" : "This is title 5",
					"image"    : "images/placeholders/news_05.jpg"
				},
				{
					"meta"     : "By Some Guy &bull; Thursday, January 9th, 2015",
					"headline" : "This is title 6",
					"image"    : "images/placeholders/news_06.jpg"
				},
				{
					"meta"     : "By Some Guy &bull; Thursday, January 9th, 2015",
					"headline" : "This is title 7",
					"image"    : "images/placeholders/news_07.jpg"
				},
				{
					"meta"     : "By Some Guy &bull; Thursday, January 9th, 2015",
					"headline" : "This is title 8",
					"image"    : "images/placeholders/news_08.jpg"
				},
				{
					"meta"     : "By Some Guy &bull; Thursday, January 9th, 2015",
					"headline" : "This is title 9",
					"image"    : "images/placeholders/news_09.jpg"
				}
			]
		},
		breakingHtml = breakingTemplate( breakingContent );
	$( '.slider--news' ).html( breakingHtml );

	var sportsEntertainment = $( '.section--entertainment' ).html(),
		sportsTemplate = Handlebars.compile( sportsEntertainment ),
		sportsContent = {
			"category" : "entertainment",
			"content"  : [
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "Jenn Sterger responsible for Oregon's 'no means no' chant",
					"image"    : "images/placeholders/entertainment_01.jpg"
				},
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "Jenn Sterger responsible for Oregon's 'no means no' chant",
					"image"    : "images/placeholders/entertainment_02.jpg"
				},
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "Jenn Sterger responsible for Oregon's 'no means no' chant",
					"image"    : "images/placeholders/entertainment_03.jpg"
				},
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "Jenn Sterger responsible for Oregon's 'no means no' chant",
					"image"    : "images/placeholders/entertainment_04.jpg"
				}
			]
		},
		sportsHtml = sportsTemplate( sportsContent );
	$( '.slider--entertainment' ).html( sportsHtml );

	var nflNews = $( '.section--nfl' ).html(),
		nflTemplate = Handlebars.compile( nflNews ),
		nflContent = {
			"category" : "nfl",
			"content"  : [
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "FSU DE Mario Edwards Entering NFL Draft",
					"image"    : "images/placeholders/nfl-news_01.jpg"
				},
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "FSU DE Mario Edwards Entering NFL Draft",
					"image"    : "images/placeholders/nfl-news_02.jpg"
				},
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "FSU DE Mario Edwards Entering NFL Draft",
					"image"    : "images/placeholders/nfl-news_03.jpg"
				},
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "FSU DE Mario Edwards Entering NFL Draft",
					"image"    : "images/placeholders/nfl-news_04.jpg"
				},
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "FSU DE Mario Edwards Entering NFL Draft",
					"image"    : "images/placeholders/nfl-news_05.jpg"
				},
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "FSU DE Mario Edwards Entering NFL Draft",
					"image"    : "images/placeholders/nfl-news_06.jpg"
				},
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "FSU DE Mario Edwards Entering NFL Draft",
					"image"    : "images/placeholders/nfl-news_07.jpg"
				}
			]
		},
		nflHtml = nflTemplate( nflContent );
	$( '.slider--nfl' ).html( nflHtml );


	var nbaNews = $( '.section--nba' ).html(),
		nbaTemplate = Handlebars.compile( nbaNews ),
		nbaContent = {
			"category" : "nfl",
			"content"  : [
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "Deron Williams Out Indefinitely With Rib Injury",
					"image"    : "images/placeholders/nba-news_01.jpg"
				},
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "Deron Williams Out Indefinitely With Rib Injury",
					"image"    : "images/placeholders/nba-news_02.jpg"
				},
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "Deron Williams Out Indefinitely With Rib Injury",
					"image"    : "images/placeholders/nba-news_03.jpg"
				},
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "Deron Williams Out Indefinitely With Rib Injury",
					"image"    : "images/placeholders/nba-news_04.jpg"
				},
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "Deron Williams Out Indefinitely With Rib Injury",
					"image"    : "images/placeholders/nba-news_05.jpg"
				},
				{
					"meta"     : "by GLENN ERBY • Thursday, January 8th, 2015",
					"headline" : "Deron Williams Out Indefinitely With Rib Injury",
					"image"    : "images/placeholders/nba-news_06.jpg"
				}
			]
		},
		nbaHtml = nbaTemplate( nbaContent );
	$( '.slider--nba' ).html( nbaHtml );
}

=======
	modifier = 1.5;
=======
var $window  = $( window ),
    modifier = 1.5;
>>>>>>> dev-angular-configs

function triggerParallax() {
	$window.bind( 'scroll', function () {
		customParallax( $window.scrollTop() );
	} );
}

function customParallax( windowScrolled ) {
	var scrlPrct = windowScrolled / 1000,
	    per      = Math.floor( 100 * scrlPrct ),
	    mover    = -( per * modifier );
	if ( mover > -140 ) {
		$( '.container--sub' ).css( { 'top' : mover + 'px' } );
	} else {
		$( '.container--sub' ).css( { 'top' : '-140px' } );
	}
}

///////////////////
function createDragger( element ) {
>>>>>>> dev-angular-configs

	var $container = element.parent(),
	    gridWidth  = $container.find( '.slider__content' ).width(),
	    totalWidth = 0;

	$container.find( '.slider__content' ).each( function () {
		totalWidth += parseInt( $( this ).width() );
	} );

	$container.css( { 'width' : totalWidth } );

<<<<<<< HEAD
	Draggable.create( container, {
		type           : 'x',
		edgeResistance : 0.65,
		bounds         : { minX : -( totalWidth - gridWidth ), maxX : 0 },
		throwProps     : true,
		//snap           : {
		//	x : function ( endValue ) {
		//		return Math.round( endValue / gridWidth ) * gridWidth;
		//	}
		//},
		onDragEnd      : function ( e ) {
			//var absEndX = Math.ceil( Math.abs( this.endX ) ),
			//	currentSlide = Math.ceil( ( absEndX / totalWidth ) * numChildren + 1 );
			//updatePagi( e.target, currentSlide );
		}
	} );

	//createPagi( container );
}

function updatePagi( element, currentSlide ) {
	var $sibPagi = $( element ).parent().parent().parent().find( '.slider-pagi' );
	$sibPagi.find( 'span' ).removeClass( 'slider-active' );
	$sibPagi.find( 'span:nth-child(' + currentSlide + ')' ).addClass( 'slider-active' );
}

function createPagi( container ) {
	var childLength = container.children().length;
	var output = '<div class="slider-pagi s' + childLength + '" style="width:' + (childLength + 1) * 15 + 'px">';
	for ( var i = 0; i < childLength - 1; i++ ) {
		if ( i === 0 ) {
			output += '<span class="slider-pagi--tab slider-active"></span>';
		}
		output += '<span class="slider-pagi--tab"></span>';
	}
	output += '</div>';
	container.parent().append( output );
=======
	Draggable.create( $container, {
		type            : 'x',
		edgeResistance  : 0.65,
		minimumMovement : 25,
		bounds          : { minX : -( totalWidth - gridWidth ), maxX : 0 },
		throwProps      : true
	} );
}

$( function () {

	var $body = $( 'body' );
	/**
	 * creating a button listener to the body element
	 * so when button is created dynamically, it will function
	 */
	$body.on( 'click', '.button--close', function ( e ) {
		e.preventDefault();
		window.history.back();
	} );

	$body.on( 'click', '.social--twitter', function ( e ) {
		e.preventDefault();
		var $this   = $( this ),
		    $parent = $this,
		    $image  = $parent.siblings( '.thumbnail' );

		if ( !$parent.hasClass( 'dim' ) ) {
			$parent
				.css( {
					opacity : '.35'
				} )
				.addClass( 'dim' );
			$image
				.css( {
					opacity : '1'
				} );
		} else {
			$parent
				.css( {
					opacity : '1'
				} )
				.removeClass( 'dim' );
			$image
				.css( {
					opacity : '.35'
				} );
		}
	} );

} );

function createPulldown() {
	var $menu        = $( '.nav__pulldown-area' ),
	    $menuItems   = $menu.find( 'a' ),
	    $menuHeight  = $menu.height(),
	    $pullDownBtn = $( '.nav__current-page' );

	CSSPlugin.defaultTransformPerspective = 300;
	TweenLite.set( $menu, { height : 0, autoAlpha : 0 } );
	TweenLite.set( $menuItems, { autoAlpha : 0 } );
	var menuTL = new TimelineMax();
	menuTL.to( $menu, 0.5, { height : $menuHeight, autoAlpha : 1, ease : Strong.easeOut } )
		.staggerTo( $menuItems, 0.5, { autoAlpha : 1, x : 0, ease : Back.easeOut }, 0.1, '+.05' )
		.reverse();

	$pullDownBtn.on( 'click', function ( e ) {
		e.preventDefault();
		menuTL.reversed( !menuTL.reversed() )
	} );
}

function staggerIn( elem ) {
	var elems = $( elem ).parent().children( '.content' );
	TweenMax.staggerFrom( elems, .75, { left : '200%', ease : Back.easeOut }, 0.15 );
}

function plopIn( elem ) {
	var elems = $( elem ).parent().children( '.segment__content' );
	elems.each( function () {
		var elementOffset = $( this ).offset(),
		    offset        = elementOffset.top + elementOffset.left,
		    delay         = ( offset / 1000 ) + 0.5;
		TweenMax.from( $( this ), 0.5, { scale : 0, delay : delay, ease : Back.easeOut } );
	} )
}

function slideIn( elem ) {
	var elems = $( elem ).parent().find( '.slider' );
	TweenMax.staggerFrom( elems, .75, { left : '200%', delay : 1.2, ease : Back.easeOut }, 0.15 );
>>>>>>> dev-angular-configs
}
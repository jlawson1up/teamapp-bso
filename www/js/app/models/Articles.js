angular.module( 'affiliate.modules.articles', [
	'affiliate.factories'
] )

/**
 * ArticlesModel :
 *
 * Gets a list of endpoints, requests information, returns all ( in order ) when done
 */
	.service( 'ArticlesModel', function ( $http, $q, AppCache ) {
		var model = this;

		/**
		 *
		 * fetchArticle : ( private )
		 *
		 * request information and returns the data from the array when complete
		 *
		 * @param endpoint
		 * @return {String}
		 */
		var fetchArticle = function fetchArticle( endpoint ) {
			return $http.get( endpoint, { cache : AppCache } )
				.then( function ( data ) {
					return data.data;
				} );
		};

		/**
		 * getArticles : ( public )
		 *
		 * Receives list of endpoints, calls for data, returns to controller when all complete
		 *
		 * @param endpoints
		 * @return {Array} of all data that has come back from the endpoints
		 */
		model.getArticles = function ( endpoints ) {
			var allEndPoints = _.map( endpoints, function ( element ) {
				return fetchArticle( element )
			} );

			return $q.all( allEndPoints )
				.then( function ( resultsArray ) {
					return resultsArray;
				} );
		};

		/**
		 * getSingleArticle : ( public )
		 *
		 * Receives an endpoint, calls for data, returns to controller when complete
		 *
		 * @param endpoint
		 * @return {Array} of all data that has come back from the endpoints
		 */
		model.getSingleArticle = function( endpoint ){
			return fetchArticle( endpoint );
		};

	} );
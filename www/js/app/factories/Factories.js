/**
 * This is a common file for all app factories.
 */
angular.module( 'affiliate.factories', [] )

/**
 * AppCache:
 * The sole purpose of this factory is to have single point of cache
 * that can be cleared at any time.
 */
	.factory( 'AppCache', function ( $cacheFactory ) {
		return $cacheFactory( 'app' )
	} );

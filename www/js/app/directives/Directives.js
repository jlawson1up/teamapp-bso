/**
 * This is a common file for all app directives
 */
angular.module( 'affiliate.directives', [] )

/**
 * bgImage:
 *
 * this is for any container that an image needs to be applied.
 * doing the normal style='background-image : url();' will result in an
 * error being thrown for the first image.
 * ( similar issue for <img src=''> solution is <img ng-src=''>
 */
	.directive( 'bgImage', function () {
		return function ( scope, elem, attrs ) {
			scope.$watch( attrs.bgImage, function ( value ) {
				//console.log( 'image ' + value );
				if ( value !== undefined ) {
					elem.css( {
						'background-image' : 'url(' + value + ')'
					} );
				} else {
					elem.css( {
						'background-image' : 'url(/images/placeholder-black.jpg)'
					} );
				}
			} );
		};
	} )

	.directive( 'timeAgo', function () {
		return {
			restrict : 'A',
			link     : function ( scope, elem, attrs ) {
				scope.$watch( attrs.timeAgo, function ( value ) {
					var now = moment(new Date());
					var postTime = moment( value );
					elem.html( postTime.from( now ) );
				} );
			}
		};
	} )

/**
 * errSrc:
 *
 * This is a good fall back solution, if an image src returns in a 404,
 * I can place a default image
 */
	.directive( 'errSrc', function () {
		return {
			link : function ( scope, elem, attrs ) {
				attrs.$observe( 'ngSrc', function ( value ) {
					if ( !value && attrs.errSrc ) {
						attrs.$set( 'src', attrs.errSrc );
					}
				} );
			}
		}
	} )

/**
 * addDraggable:
 *
 * This directive tells me the end of each section on the home page
 * allowing me to trigger the creation of the draggable area.
 */
	.directive( 'addDraggable', function () {
		return {
			restrict : 'A',
			link     : function ( scope, elem, attrs ) {
				if ( scope.$last ) {
					createDragger( elem );
				}
			}
		};
	} )

/**
 * staggerIn:
 *
 * This directive tells me the end of section on the list view
 * allowing me to trigger the animation effect desired.
 */
	.directive( 'staggerIn', function () {
		return {
			restrict : 'A',
			link     : function ( scope, elem, attrs ) {
				if ( scope.$last ) {
					var id = $( attrs.$$element ).parent().attr( 'id' );
					if ( !scope.isLoaded[ id ] ) {
						staggerIn( elem );
						scope.isLoaded[ id ] = true;
						//console.log( scope.isLoaded, 'from staggerIn' )
					}
				}
			}
		}
	} )

/**
 * plopIn:
 *
 * This directive tells me the end of section on the list view
 * allowing me to trigger the animation effect desired.
 */
	.directive( 'plopIn', function () {
		return {
			restrict : 'A',
			link     : function ( scope, elem, attrs ) {
				if ( scope.$last ) {
					var id = $( attrs.$$element ).parent().attr( 'id' );
					if ( !scope.isLoaded[ id ] ) {
						plopIn( elem );
						scope.isLoaded[ id ] = true;
						//console.log( scope.isLoaded, 'from plopIn' )
					}
				}
			}
		}
	} )

/**
 * slideIn:
 *
 * This directive tells me the end of section on the list view
 * allowing me to trigger the animation effect desired.
 */
	.directive( 'slideIn', function () {
		return {
			restrict : 'A',
			link     : function ( scope, elem, attrs ) {
				if ( scope.$last ) {
					var id = $( attrs.$$element ).parent().attr( 'id' );
					if ( !scope.isLoaded[ id ] ) {
						slideIn( elem );
						scope.isLoaded[ id ] = true;
						//console.log( scope.isLoaded, 'from slideIn' );
					}
				}
			}
		}
	} )

/**
 * slideIn:
 *
 * This directive tells me the end of section on the list view
 * allowing me to trigger the animation effect desired.
 */
	.directive( 'triggerParallax', function () {
		return {
			restrict : 'A',
			link     : function ( scope, elem, attrs ) {
				if ( scope.$last ) {
					triggerParallax();
				}
			}
		}
	} )
;
angular.module( 'affiliate.contents', [
	'affiliate.modules.articles'
] )

/**
 * Connects data to the content view
 */
	.controller( 'ContentCtrl', function ( $scope, $routeParams, ArticlesModel ) {
		var contentCtrl = this;
		console.log( 'In ContentCtrl' );

		contentCtrl.content = _.findWhere( $scope.appContent, { title : $routeParams.sectionId } );
		contentCtrl.whichContent = $routeParams.articles || 0;

		/**
		 * Call to the ArticlesModel to fetch all the content from the endpoints
		 */
		ArticlesModel.getArticles( _.pluck( contentCtrl.content.sections, 'endpoint' ) )
			.then( function ( data ) {

				_.each( contentCtrl.content.sections, function ( object, index ) {
					object.articles = data[ index ]
				} );

				var id = contentCtrl.content.sections[ contentCtrl.whichContent ].title;
				console.log( id );
				if ( !$scope.isRandom[ id ] ) {
					_.each( contentCtrl.content.sections, function ( object, index ) {
						object.article = _.map( data[ index ], function ( article ) {
							article.randomSize = $scope.getRandomSize();
							//console.log( article.randomSize );
							return article;
						} );
					} );
					$scope.isRandom[ id ] = true;
					console.log( $scope.isRandom );
				}
			} );
		createPulldown();
	} );
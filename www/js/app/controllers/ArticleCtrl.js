angular.module( 'affiliate.article', [
	'affiliate.modules.articles'
] )

/**
 * Connects data to the article view
 */
	.controller( 'ArticleCtrl', function ( $scope, $routeParams, $sce, ArticlesModel ) {
		var articleCtrl = this;
		console.log( 'In ArticleCtrl' );

		var sectionId = $routeParams.sectionId,
			subsectionIndex = $routeParams.subsectionIndex,
			articleIndex = $routeParams.articleIndex;

		articleCtrl.content = _.findWhere( $scope.appContent, { title : sectionId } );
		var currentArticle;


		/**
		 * Check to see if the article is undefined.
		 * If yes, the go fetch the one article.
		 * Else load article from memory.
		 */

		if ( articleCtrl.content[ 'sections' ][ subsectionIndex ][ 'articles' ] === undefined ) {
			ArticlesModel.getSingleArticle( articleCtrl.content[ 'sections' ][ subsectionIndex ].endpoint )
				.then( function ( data ) {
					currentArticle = data[ articleIndex ];
					articleCtrl.title = currentArticle.title;
					articleCtrl.trustHtml = function () {
						return $sce.trustAsHtml( currentArticle.text );
					};
				} );
		} else {
			currentArticle = articleCtrl.content[ 'sections' ][ subsectionIndex ][ 'articles' ][ articleIndex ];
			articleCtrl.title = currentArticle.title;
			articleCtrl.trustHtml = function () {
				return $sce.trustAsHtml( currentArticle.text );
			};
		}


	} );
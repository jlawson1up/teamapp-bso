angular.module( 'affiliate.socials', [
	'affiliate.modules.articles'
] )

/**
 * Connects data to the social view
 */
	.controller( 'SocialCtrl', function ( $scope, $routeParams, ArticlesModel ) {
		var socialCtrl = this;
		console.log( 'In SocialCtrl' );

		socialCtrl.content = _.findWhere( $scope.appContent, { title : $routeParams.sectionId } );
		socialCtrl.whichContent = $routeParams.articles || 0;

		/**
		 * Call to the ArticlesModel to fetch all the content from the endpoints
		 */
		ArticlesModel.getArticles( _.pluck( socialCtrl.content.sections, 'endpoint' ) )
			.then( function ( data ) {
				_.each( socialCtrl.content.sections, function ( object, index ) {
					object.articles = data[ index ];
					console.log( socialCtrl.content );
				} );
			} );
		createPulldown();
	} );